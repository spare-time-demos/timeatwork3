/// <reference types="cypress" />

context('timeatwork3 teammember rest endpoint', () => {
  
  it('REST Endpoint does NOT work currently', () => {
    cy.log('will fail currently on gitlab-ci ... make it work!')
    cy.request('teammember', {failOnStatusCode: false}).then((response) => {
      expect(response.status).to.eq(200)
    })
  })

})