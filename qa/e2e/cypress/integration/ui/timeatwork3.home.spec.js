/// <reference types="cypress" />

context('timeatwork3 html', () => {
  
  beforeEach(() => {
    cy.visit('')
  })

  it('is generic react page shown on root', () => {
    cy.get('h1').contains('timeatwork3').should('exist')
  })

})