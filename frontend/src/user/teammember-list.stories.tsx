import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';

import TeamMemberList from './teammember-list';

export default {
    title: 'Timeatwork3/User/TeamMemberList',
    component: TeamMemberList,
} as Meta;

const Template: Story<any>  = (args) => <TeamMemberList {...args}/>;


export const Primary = Template.bind({});
Primary.args = {
};