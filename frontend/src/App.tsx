import React from 'react';
import './App.css';
import TeamMemberList from './user/teammember-list';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          timeatwork3
        </h1>
      </header>
      <p>a basic react frontend</p>

      <TeamMemberList />
    </div>      
  );
}

export default App;
