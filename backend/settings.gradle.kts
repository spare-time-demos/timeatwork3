pluginManagement {

    val quarkusPluginVersion: String by settings
    val quarkusPluginId: String by settings

    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
    }
    
    plugins {
        id(quarkusPluginId) version quarkusPluginVersion
    }
}

// gradle settings file timeatwork2
rootProject.name= "backend"
