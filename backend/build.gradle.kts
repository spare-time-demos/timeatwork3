// timeatwork2 gradle build file
// usage: 
//
// - gradlew wrapper --gradle-version 6.5.1
// - gradlew dropAll update dbDoc
// - gradlew test
// - gradlew quarkusDev

val quarkusPluginVersion:       String by project
val quarkusPlatformArtifactId:  String by project
val quarkusPlatformGroupId:     String by project
val quarkusPlatformVersion:     String by project

plugins {
	id("java")
	id("io.quarkus")
	id("org.liquibase.gradle")  version "2.0.4"
    id("org.openapi.generator") version "4.3.1"
}

repositories {
     mavenLocal()
     mavenCentral()
	 jcenter()
}

dependencies {    
  	liquibaseRuntime("org.liquibase:liquibase-core:4.1.1")	{ because("database migrations with liquibase on postgress") }						     
    liquibaseRuntime("org.postgresql:postgresql:42.2.18")   { because("database migrations database driver postresql   ") }    
	liquibaseRuntime("org.yaml:snakeyaml:1.27")				{ because("liquibase yaml configuration") }

    implementation("io.quarkus:quarkus-config-yaml")
    implementation("io.quarkus:quarkus-hibernate-orm")
    implementation("io.quarkus:quarkus-hibernate-orm-panache")
    implementation("io.quarkus:quarkus-jdbc-postgresql")
    implementation("io.quarkus:quarkus-liquibase")
    implementation("io.quarkus:quarkus-resteasy-jackson")
    implementation("io.quarkus:quarkus-resteasy")
    implementation("io.quarkus:quarkus-smallrye-openapi")
    implementation("io.quarkus:quarkus-micrometer")
    implementation("io.quarkus:quarkus-smallrye-opentracing")
    implementation(enforcedPlatform("${quarkusPlatformGroupId}:${quarkusPlatformArtifactId}:${quarkusPlatformVersion}"))

    testImplementation("io.quarkus:quarkus-junit5")
    testImplementation("io.rest-assured:rest-assured")
    testImplementation("org.assertj:assertj-core") // :3.14.0'						// test assertion library

}

group   = "org.manathome.timeatwork"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}


tasks {
    test {
        systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")
        ignoreFailures = true
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
}    

tasks.register<Copy>("copyFrontendIntoBackend") {
    // copy react client code into backend resources for assembly
    from("$projectDir/../frontend/build/")
    into("$buildDir/resources/main/META-INF/resources")
}


liquibase {
	// database management
	activities.register("postgres") {
		this.arguments = mapOf(
		 	"changeLogFile"  to "src/main/resources/db/changelog/db.changelog-master.yaml",
            "logLevel"       to "warn",
//          "url"            to "jdbc:postgresql://localhost:5432/postgres",
		 	"url"            to "jdbc:postgresql://postgres:5432/postgres",
		 	"username"       to "postgres",
		 	"password"       to "postgres",
		 	"driver"         to "org.postgresql.Driver"
			)
	}
}


openApiGenerate  {
    // generate typescript client for rest api for vue frontend
    generatorName.set("typescript-fetch")
    inputSpec.set("$projectDir/time@work3.backend.openapi.schema.yaml")
    outputDir.set("$projectDir/../frontend/src/restclient")
    verbose.set(false)
}