package org.manathome.timeatwork.util;

/** internal coding/unexpected technical error. */
public class TimeAtWorkInternalException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public TimeAtWorkInternalException() {
    super();
  }

  public TimeAtWorkInternalException(final String message) {
    super(message);
  }

  public TimeAtWorkInternalException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
