package org.manathome.timeatwork.db.repository;

import javax.enterprise.context.ApplicationScoped;

import org.manathome.timeatwork.domain.TeamMember;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class TeamMemberRepository implements PanacheRepository<TeamMember>  {
  
  
  TeamMember findByUserId(String userId) {
	  return find("userId", userId).firstResult();
  }
  
}
