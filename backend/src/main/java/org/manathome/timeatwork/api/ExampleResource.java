package org.manathome.timeatwork.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@Path("/hello")
public class ExampleResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(summary = "dummy endpoint, returns hello")
    @APIResponse(responseCode = "200", description = "static text hello", content = @Content(mediaType = "text/hml"))
    public String hello() {
        return "hello";
    }
}