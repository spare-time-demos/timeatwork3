package org.manathome.timeatwork.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;

import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.TeamMember;

@Path("/teammember")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TeamMemberResource {

	@Inject
	TeamMemberRepository tmRepository;
	
    @GET
    @Operation(summary = "get all teammembers", operationId = "getAllTeamMembers")
    public List<TeamMember> list() {
        return tmRepository.listAll();
    }
}