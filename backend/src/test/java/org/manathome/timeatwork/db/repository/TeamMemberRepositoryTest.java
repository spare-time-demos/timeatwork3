package org.manathome.timeatwork.db.repository;

import static org.assertj.core.api.Assertions.*;

import java.util.Date;

import javax.inject.Inject;
import javax.persistence.LockModeType;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.TeamMember;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class TeamMemberRepositoryTest {

	@Inject
	TeamMemberRepository repository;
	
	@Test
    public void testAccessStoredTeamMembers() {
		final var rowCount = repository.count();
		assertThat(rowCount).as("table row count").isGreaterThan(0);
    }

	@Test
    public void testLoadTeamMemberFromDatabase() {
		
		final var tlead1 = repository.findByUserId("tlead1");
		
		assertThat(tlead1).as("loaded tlead1").isNotNull();
		assertThat(tlead1.getName()).as("tlead1 name").contains("a first test lead and user");
		assertThat(tlead1.isLocked()).as("not locked").isFalse();
		assertThat(tlead1.isActive()).as("is active").isTrue();
		assertThat(tlead1.isLeadRole()).as("is lead").isTrue();
    }

	@Test
	@TestTransaction 
    public void testAddTeamMemberToDatabase() {
		
		final var tm = new TeamMember("junit-data", "team member by junit" + new Date());
		
		repository.persist(tm);
		assertThat(repository.isPersistent(tm)).as("stored team member").isTrue();
		assertThat(tm.getId()).isGreaterThan(1);
		
		final var tmRead = repository.findById(tm.getId(), LockModeType.NONE);
		assertThat(tmRead).isNotNull();
		assertThat(tmRead.getName()).isEqualTo(tm.getName());
    }	


	@Test
	@TestTransaction 
    public void testDeleteTeamMemberFromDatabase() {
		
		final var tm = new TeamMember("junit-data", "tm junit to be deleted" + new Date());
		
		repository.persist(tm);
		
		final var id = tm.getId();
		
		repository.delete(tm);
		assertThat(tm).isNotNull();
		assertThat(repository.isPersistent(tm)).isFalse();
		
		final var tmRead = repository.findById(id, LockModeType.NONE);
		assertThat(tmRead).isNull();
    }

	@Test
	@TestTransaction 
    public void testChangeTeamMemberInDatabase() {
		
		final var tm = new TeamMember("junit-data", "tm junit to be changed" + new Date());		
		repository.persist(tm);		
		repository.flush();
				
		final var tmToChange = repository.findById(tm.getId(), LockModeType.PESSIMISTIC_WRITE);
		tmToChange.setName("tm junit now changed" );
		tmToChange.setActive(false);
		repository.persist(tm);		
		repository.flush();
		
		final var tmRead = repository.findById(tm.getId(), LockModeType.NONE);
		assertThat(tmRead.isActive()).isFalse();		
    }
}
