package org.manathome.timeatwork.api;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
public class TeamMemberResourceTest {

    @Test
    public void testTeamMemberList() {
    	
         var body = given()
          .when().get("/teammember")
          .then()
          	 .log().ifValidationFails()
             .statusCode(200)
           .and()
             .body("[0].id", equalTo(-1))
             .body("[0].userId", equalTo("tlead1"))
             .body("[0].name", equalTo("a first test lead and user"))
             .body("[0].locked", equalTo(false))
             .body("[0].active", equalTo(true))
             .body("[0].leadRole", equalTo(true))
             .body("[0].adminRole", equalTo(false))
          .and()
          .extract()
          	.asString();
         
         System.out.println(body);
    }

}